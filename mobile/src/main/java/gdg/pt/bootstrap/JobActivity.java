package gdg.pt.bootstrap;

import android.os.Bundle;
import android.support.annotation.Nullable;

import java.util.List;

import gdg.pt.common.data.contracts.JobOffer;
import gdg.pt.common.internet.retrofit.LandingJobsServer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created on 09-12-2016.
 */

public class JobActivity extends BaseActivity
{
	private List<JobOffer> jobOffers;

	@Override protected void onCreate( @Nullable Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );

		setContentView( R.layout.activity_job );

		this.initializeData();
	}

	private void initializeData()
	{
		Call< List< JobOffer > > jobs = LandingJobsServer.getCompanyServerRestAdapter().getJobOffers();

		jobs.enqueue( new Callback< List< JobOffer > >()
		{
			@Override
			public void onResponse( Call< List< JobOffer > > call, Response< List< JobOffer > > response )
			{
				jobOffers = response.body();
				if( jobOffers != null && jobOffers.size() > 0 )
				{
					setData(jobOffers.get( 0 ));;
				}
			}

			@Override public void onFailure( Call< List< JobOffer > > call, Throwable t )
			{

			}
		} );
	}

	private void setData( JobOffer job )
	{

	}
}
