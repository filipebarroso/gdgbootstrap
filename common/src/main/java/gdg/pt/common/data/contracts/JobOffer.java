package gdg.pt.common.data.contracts;

import com.google.gson.annotations.SerializedName;


/**
 * Created on 09-12-2016.
 */

public class JobOffer
{
	@SerializedName( "" ) String name;

	public String getName()
	{
		return name;
	}
	public void setName( String name )
	{
		this.name = name;
	}
}
