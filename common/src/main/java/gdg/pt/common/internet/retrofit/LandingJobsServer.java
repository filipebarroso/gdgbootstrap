package gdg.pt.common.internet.retrofit;

import gdg.pt.common.internet.CompanyConstants;
import gdg.pt.common.internet.retrofit.api.LandingJobsApi;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;


/**
 * Created on 09-12-2016.
 */

public class LandingJobsServer
{
	private static LandingJobsApi landingJobsApi;

	public static LandingJobsApi getCompanyServerRestAdapter()
	{
		if( landingJobsApi == null )
		{
			OkHttpClient client = new OkHttpClient();

			Retrofit retrofit = new Retrofit.Builder().client( client )
					.baseUrl( CompanyConstants.COMPANY_API_ENDPOINT )
					.build();
			landingJobsApi = retrofit.create( LandingJobsApi.class );
		}

		return landingJobsApi;
	}
}
