package gdg.pt.common.internet.retrofit.api;

import java.util.List;

import gdg.pt.common.data.contracts.JobOffer;
import retrofit2.Call;
import retrofit2.http.GET;


/**
 * Created on 09-12-2016.
 */

public interface LandingJobsApi
{
	@GET("") Call<List<JobOffer >> getJobOffers();
}
